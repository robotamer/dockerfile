Citadel Mail Server
===================

From the website
----------------
[Citadel](http://www.citadel.org) is a highly integrated Groupware Platform with a AJAX-powered “Web 2.0” interface, but also providing SMTP, IMAP, POP3 and GroupDAV access to its content.

Citadel offers versatile email services with very low administration needed. 
It provides its own implementations of these server protocols: 
IMAP, POP3, SMTP, ManageSieve, XMPP, Citadel.

It is 100% free open source, its codebase is written mostly in C. 

Has client libraries for:

 - Python
 - Perl  
 - PHP  
 - Golang  


Docker image specific HowTo
===========================

After building enter your citadel container, and run `dpkg-reconfigure` on `citadel-server` to set your 
AIDE/Admin password.  

citadel create
```
	docker run -it --name="citadel" --hostname="mail.riky.net" acc12703ecf1 bash
	dockersetlocalhosts v
	dpkg-reconfigure citadel-server
	doctrl clean
	doctrl start
	^P ^Q (exit)
```

citadel start up
```
	docker start citadel
	dockersetlocalhosts v
	docker attach citadel
	doctrl start
	^P ^Q (exit)
```

Exit the container with `^p ^q`

Open a web browser and do go the containers ip address



systemd script
```
[Unit]
Description=Citadel container
Author=RoboTamer
After=docker.service
Requires=docker.service

[Service]
Restart=always
ExecStart=/usr/bin/docker start -a citadel
ExecStop=/usr/bin/docker stop -t 2 citadel
ExecStopPost=/bin/bash -c "/bin/systemctl stop docker-$(docker ps -a --no-trunc |grep service |awk '{print $1}').scope"

[Install]
WantedBy=local.target
```


User Feedback
-------------
If you have any problems with or questions about this image, please contact us through a 
[bitbucket](https://bitbucket.org/robotamer/dockerfile/issues) issue.

Official citadel
----------------
You can also reach many of the [official citadel maintainers](http://uncensored.citadel.org) at the citadel uncensored bbs.

Contributing
------------
You are invited to contribute new features, fixes, or updates, large or small; we are always thrilled to receive pull requests.

100% open source
----------------
GNU General Public License

[Citadel]:(http://www.citadel.org "Citadel")